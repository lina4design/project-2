import * as firebase from "firebase";
import ReactDOM from "react-dom";
import React from "react";
import "./css/basic.css";
import "./css/style.css";
import "./css/utils.css";
import "./css/vendor/bootstrap-grid.min.css";
import "./css/vendor/bootstrap-reboot.min.css";
import "./css/vendor/icons/pe-icon-7-stroke/css/helper.css";
import "./css/vendor/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { NAV_MAP } from "./globals";

class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          {NAV_MAP.map(({ path, page }) => (
            <Route path={path} key={path} exact render={page} />
          ))}
        </div>
      </Router>
    );
  }
}
//=============
ReactDOM.render(<App />, document.getElementById("root"));
