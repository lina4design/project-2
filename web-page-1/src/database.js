import * as firebase from "firebase";
import Rebase from "re-base";
import { FIREBASE_CONFIG } from "./globals";

const app = firebase.initializeApp(FIREBASE_CONFIG);
const base = Rebase.createClass(app.database());
export { base };
