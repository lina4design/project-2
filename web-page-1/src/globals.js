import React from "react";
import { About, Contacts, Home } from "./components";

export { default as LOGO } from "./images/logo.png";
export const GOOGLE_API_KEY = "AIzaSyBBYO3snN1YHWANBjVi2IwazhOWRdbC5Jo";

export const NAV_MAP = [
  {
    path: "/",
    title: "Home",
    page: () => <Home />
  },
  {
    path: "/about",
    title: "About",
    page: () => <About />
  },
  {
    path: "/contacts",
    title: "Contacts",
    page: () => <Contacts />
  }
];

export const FIREBASE_CONFIG = {
    apiKey: "AIzaSyBbjDRp9Fivw3g6UktnSF0ooaAcq5lAbQw",
    authDomain: "react-app-1-c80ca.firebaseapp.com",
    databaseURL: "https://react-app-1-c80ca.firebaseio.com",
    projectId: "react-app-1-c80ca",
    storageBucket: "",
    messagingSenderId: "554976150303"
  };