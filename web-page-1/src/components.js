import React from "react";
import { NavLink } from "react-router-dom";
import { LOGO, GOOGLE_API_KEY, NAV_MAP } from "./globals";
import { base } from "./database.js";
/**
 *
 *
 * @class Header sukuria navigaciją
 * @extends {React.Component}
 */
export class Header extends React.Component {
  render() {
    return (
      <nav className="d-flex align-items-center ">
        <img src={LOGO} alt="" className="logo" />
        <ul className="navigation">
          {NAV_MAP.map(({ path, title }) => (
            <li>
              <NavLink to={path} exact key={title}>
                {title}
              </NavLink>
            </li>
          ))}
        </ul>
      </nav>
    );
  }
}
/**
 *
 *
 * @class Section sukuria konteinerį tekstui arba kitiems
 * konteineriams talpinti
 * @extends {React.Component}
 * @prop {importance} - ant kiek svarbi antraštė 1=><h1>, 2=><h2>...
 * @prop {}
 */
export class Content extends React.Component {
  render() {
    const { importance: imp, header, text, position } = this.props;
    const HeaderTag = `h${imp}`;
    switch (position) {
      case "left":
        return (
          <div className="row no-gutters">
            <div className="col content">
              <HeaderTag>{header}</HeaderTag>
              <p>{text}</p>
            </div>
            {this.props.children}
          </div>
        );
      case "right":
        return (
          <div className="row">
            {this.props.children}
            <div className="col content">
              <HeaderTag>{header}</HeaderTag>
              <p>{text}</p>
            </div>
          </div>
        );
      case "center":
        return (
          <div className="row">
            <div className="col-10 flex-col-center -container">
              <HeaderTag>{header}</HeaderTag>
              <p>{text}</p>
            </div>
            {this.props.children}
          </div>
        );
      default:
        return <div className="row no-gutters">{this.props.children}</div>;
    }
  }
}
export class Section extends React.Component {
  render() {
    const { color } = this.props;
    return (
      <div className={`-container ${color}-- p-5`}>{this.props.children}</div>
    );
  }
}
export class ImageSection extends React.Component {
  render() {
    const { color } = this.props;
    return <div className={`${color}--`}>{this.props.children}</div>;
  }
}

export const IconBlock = ({ icon, number, text, ...otherProps }) => (
  <div className={"col p-0"} {...otherProps}>
    <i className={`icon pe-lg pe-3x pe-7s-${icon}`} />
    <p className={"icon-text"}>
      <strong>{number}</strong>
      <br />
      {text}
    </p>
  </div>
);
export const SmallIconBlock = ({ icon, text, position }) => {
  if (position === "left") {
    return (
      <div className="col p-0 d-flex align-items-center">
        <i className={`p-1 pe-lg pe-7s-${icon}`} />
        <p className="display-ib m-0 p-2">{text}</p>
      </div>
    );
  } else
    return (
      <div className={"col p-0 d-flex align-items-center"}>
        <p className="display-ib m-0 p-2">{text}</p>
        <i className={`p-1 pe-lg pe-7s-${icon}`} />
      </div>
    );
};
export class Footer extends React.Component {
  render() {
    return (
      <footer>
        <div className="row footer-container no-gutters">
          <div className="col">
            <h5>PHXdesign, Inc.</h5>
            <small> &copy; 2018 PHXdesign. All rights reserved.</small>
          </div>
          <div className="col">
            <ul>
              <li>
                <small>hello@phxdesign.com</small>
              </li>
              <li>
                <small>+44 987 065 908</small>
              </li>
            </ul>
          </div>
          <div className="col">
            <ul>
              <li>
                <small>Projects</small>
              </li>
              <li>
                <small>About</small>
              </li>
              <li>
                <small>Services</small>
              </li>
              <li>
                <small>Carrer</small>
              </li>
            </ul>
          </div>
          <div className="col">
            <ul>
              <li>
                <small>Facebook</small>
              </li>
              <li>
                <small>Twitter</small>
              </li>
              <li>
                <small>Instagram</small>
              </li>
              <li>
                <small>Dribbble</small>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    );
  }
}
export class About extends React.Component {
  render() {
    const blocks = [
      { icon: "portfolio", number: 265, text: "some text" },
      { icon: "clock", number: 546, text: "more text" },
      { icon: "star", number: 135, text: "some..." },
      { icon: "like", number: 134, text: "text..." }
    ];

    return (
      <div>
        <Header />
        <Section color="dark">
          <Content
            importance={1}
            header="Welcome!"
            text="Nice to see you!"
            position="center"
          />
        </Section>
        <Section color="color">
          <Content>
            {blocks.map(b => <IconBlock {...b} key={b.icon} />)}
          </Content>
        </Section>
        <ImageSection color="light">
          <Content
            importance={2}
            header="About me"
            text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            position="right"
          >
            <div className="image col" />
          </Content>
        </ImageSection>
        <Section>
          <Content
            importance={2}
            header="Need something?"
            text="Then write to me."
            position="center"
          />
          <button className="btn">Contact</button>
        </Section>
        <Footer />
      </div>
    );
  }
}
export class Post extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <ImageSection color="light">
          <Content
            importance={1}
            header="Header"
            text="We may think that Board should just
        ask each Square for the Square’s state. Although this approach
        is possible in React, we discourage it because the code becomes
        difficult to understand, susceptible to bugs, and hard to refactor.
        Instead, the best approach is to store the game’s state in the parent
        mponent instead of in each Square. The Board component can tell
        each Square what to display by passing a prop, just like we did
        when we passed a number to each Square."
            position="left"
          >
            <div className="image col" />
          </Content>
        </ImageSection>
        <div className="d-flex justify-content-between align-items-center p-1">
          <div>
            <SmallIconBlock
              icon="angle-left"
              text="Previous"
              position="left"
            />
          </div>
          <i className="pe-lg pe-7s-keypad" />
          <div>
            <SmallIconBlock
              icon="angle-right"
              text="Next"
              position="right"
            />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
export class Home extends React.Component {
  renderSquare() {
    return <div className="image-square col-3" />;
  }
  renderLine() {
    return (
      <div className="row">
        {this.renderSquare()}
        {this.renderSquare()}
        {this.renderSquare()}
        {this.renderSquare()}
      </div>
    );
  }
  renderPortfolio() {
    return (
      <div>
        {this.renderLine()}
        {this.renderLine()}
        {this.renderLine()}
      </div>
    );
  }
  render() {
    return (
      <div>
        <Header />
        <Section color="light">
          <Content
            importance={2}
            header="UI/UX and Graphic Designer"
            text="Change the visual order of specific flex items with a handful of order utilities. We only provide options for making an item first or last, as well as a reset to use the DOM order."
            position="center"
          />
        </Section>
        {this.renderPortfolio()}
        <Footer />
      </div>
    );
  }
}

export class ContactForm extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      mail: "",
      title: "",
      message: ""
    };
  }
  componentDidMount() {
    const baseRef =
  }
  render() {
    const { name, mail, title, message } = this.state;
    return (
      <form action="" className="p-4">
        <div className="row no-gutters">
          <div className="col m-1 ml-0">
            <input className="" type="text" value={name} placeholder="Vardas" />
          </div>
          <div className="col m-1 ml-0">
            <input className="" type="text" value={mail} placeholder="Paštas" />
          </div>
        </div>
        <div className="row no-gutters">
          <div className="col m-1 ml-0">
            <input type="text" value={title} placeholder="Pavadinimas" />
          </div>
        </div>
        <div className="m-1 ml-0">
          <textarea className="" value={message} placeholder="Žinutė" />
        </div>
        {this.props.children}
      </form>
    );
  }
}

export class Contacts extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <ImageSection color="light">
          <Content
            importance={2}
            position="left"
            header="Write to us:"
            text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
          >
            <div className="col-7">
              <ContactForm>
                <button className="btn">Contact</button>
              </ContactForm>
            </div>
            <Map ratio={1 / 3} />
          </Content>
        </ImageSection>
        <Footer />
      </div>
    );
  }
}

export const Map = ({ ratio = 16 / 9 }) => (
  <div
    className="Map"
    style={{ paddingBottom: (100 / (1 / ratio)).toString() + "%" }}
  >
    <iframe
      width="600"
      height="450"
      frameborder="0"
      src={`https://www.google.com/maps/embed/v1/place?key=${GOOGLE_API_KEY}
  &q=Space+Needle,Seattle+WA`}
      allowfullscreen
    />
  </div>
);
